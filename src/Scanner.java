import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scanner {
    private static final Map<String, TokenType> KEYWORDS;

    static {
        KEYWORDS = new HashMap<>();
        KEYWORDS.put("and", TokenType.And);
        KEYWORDS.put("class", TokenType.Class);
        KEYWORDS.put("else", TokenType.Else);
        KEYWORDS.put("false", TokenType.False);
        KEYWORDS.put("for", TokenType.For);
        KEYWORDS.put("fun", TokenType.Fun);
        KEYWORDS.put("if", TokenType.If);
        KEYWORDS.put("nil", TokenType.Nil);
        KEYWORDS.put("or", TokenType.Or);
        KEYWORDS.put("print", TokenType.Print);
        KEYWORDS.put("return", TokenType.Return);
        KEYWORDS.put("super", TokenType.Super);
        KEYWORDS.put("this", TokenType.This);
        KEYWORDS.put("true", TokenType.True);
        KEYWORDS.put("var", TokenType.Var);
        KEYWORDS.put("while", TokenType.While);
    }

    private final String source;
    private final List<Token> tokens = new ArrayList<>();
    private int start = 0;
    private int current = 0;
    private int line = 1;

    Scanner(String source) {
        this.source = source;
    }

    List<Token> scanTokens() {
        while (!isAtEnd()) {
            // We are at the beginning of the next lexeme.
            start = current;
            scanToken();
        }

        tokens.add(new Token(TokenType.EOF, "", null, line));
        return tokens;
    }

    private void scanToken() {
        char c = advance();
        switch (c) {
            case '(' -> addToken(TokenType.LeftParen);
            case ')' -> addToken(TokenType.RightParen);
            case '{' -> addToken(TokenType.LeftBrace);
            case '}' -> addToken(TokenType.RightBrace);
            case ',' -> addToken(TokenType.Comma);
            case '.' -> addToken(TokenType.Dot);
            case '-' -> addToken(TokenType.Minus);
            case '+' -> addToken(TokenType.Plus);
            case ';' -> addToken(TokenType.Semicolon);
            case '*' -> addToken(TokenType.Star);
            case '!' -> addToken(match('=') ? TokenType.BangEqual : TokenType.Bang);
            case '=' -> addToken(match('=') ? TokenType.EqualEqual : TokenType.Equal);
            case '<' -> addToken(match('=') ? TokenType.LessEqual : TokenType.Less);
            case '>' -> addToken(match('=') ? TokenType.GreaterEqual : TokenType.Greater);
            case '/' -> {
                if (match('/')) {
                    // A comment goes until the end of the line.
                    while (peek() != '\n' && !isAtEnd()) {
                        advance();
                    }
                } else {
                    addToken(TokenType.Slash);
                }
            }

            case ' ', '\r', '\t' -> {
                // Ignore whitespace.
            }
            // Newline.
            case '\n' -> ++line;
            // String literals.
            case '"' -> string();

            default -> {
                if (isDigit(c)) {
                    number();
                } else if (isAlpha(c)) {
                    identifier();
                } else {
                    Lox.error(line, "Unexpected character.");
                }
            }
        }
    }

    private void identifier() {
        while (isAlphaNumeric(peek())) {
            advance();
        }

        var text = source.substring(start, current);
        addToken(KEYWORDS.getOrDefault(text, TokenType.Identifier));
    }

    private void number() {
        while (isDigit(peek())) {
            advance();
        }

        // Look for a fractional part.
        if (peek() == '.' && isDigit(peekNext())) {
            // Consume the "."
            advance();
            while (isDigit(peek())) {
                advance();
            }
        }

        var value = source.substring(start, current);
        addToken(TokenType.Number, Double.parseDouble(value));
    }

    private void string() {
        while (peek() != '"' && !isAtEnd()) {
            if (peek() == '\n') {
                ++line;
            }
            advance();
        }

        if (isAtEnd()) {
            Lox.error(line, "Unterminated string.");
            return;
        }

        // The closing quote.
        advance();

        // Trim the surrounding quotes.
        var value = source.substring(start + 1, current - 1);
        addToken(TokenType.String, value);
    }

    private boolean match(char expected) {
        if (isAtEnd() || source.charAt(current) != expected) {
            return false;
        }

        ++current;
        return true;
    }

    private char peek() {
        return isAtEnd() ? '\0' : source.charAt(current);
    }

    private char peekNext() {
        var idx = current + 1;
        return idx >= source.length() ? '\0' : source.charAt(idx);
    }

    private boolean isAlpha(char c) {
        return c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '_';
    }

    private boolean isAlphaNumeric(char c) {
        return isAlpha(c) || isDigit(c);
    }

    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private boolean isAtEnd() {
        return current >= source.length();
    }

    private char advance() {
        return source.charAt(current++);
    }

    private void addToken(TokenType type) {
        addToken(type, null);
    }

    private void addToken(TokenType type, Object literal) {
        var text = source.substring(start, current);
        tokens.add(new Token(type, text, literal, line));
    }
}
